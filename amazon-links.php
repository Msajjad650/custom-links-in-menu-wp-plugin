<?php

/**
 * @package PhloxPlugin
 */

 /*
 Plugin Name: Amazon Links Plugin
 Plugin URI: htttp://phlox.solutions
 Description: First custom plugin
 Version: 1.0.0
 Author: Muhammad Sajjad
Author URI: http://msajjad650.move.pk
License: GPLv2 or later
Text Domain: phlox-plugin
 */

 /*
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the free software foundation; either version 2 of the license.
 */


/*if(!defined('ABSPATH')){
    die;
}*/

defined('ABSPATH') or die('Access Denied');
/*if(!function_exists('add_action')){
    echo 'Access Denied';
    die;
}*/
if(file_exists(dirname(__FILE__).'/vendor/autoload.php')){
	require_once dirname(__FILE__).'/vendor/autoload.php';
}

define('PLUGIN_PATH', plugin_dir_path(__FILE__));
define('PLUGIN_URL', plugin_dir_url( __FILE__ ));
define('PLUGIN', plugin_basename( __FILE__ ));



function activate_phlox_plugin(){
	Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_phlox_plugin' );

function deactivate_phlox_plugin(){
	Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_phlox_plugin' );


if (class_exists('Inc\\Init')) {
	Inc\Init::register_services();
}