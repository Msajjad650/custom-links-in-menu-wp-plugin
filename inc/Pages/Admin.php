<?php
/**
 * @package PhloxPlugin
 */


namespace Inc\Pages;
class Admin{

	public function register(){
		add_action( 'admin_menu', array($this, 'add_admin_pages'));
	}

	public function add_admin_pages(){
    	add_menu_page( 
    		'Phlox Plugin',
    	 	'Phlox', 
    	 	'manage_options', 
    	 	'phlox_plugin', 
    	 	array($this, 'admin_index'), 
    	 	'dashicons-store', 
    	 	110 
    	 );
    	add_submenu_page( 
    		'phlox_plugin', 
    		'Add Custom Fields', 
    		'Add Custom Fields', 
    		'manage_options', 
    		'add-custom-fields',
    		array($this, 'add_custom_field'),
    		''
    	);
    }

    public function admin_index(){
    	require_once PLUGIN_PATH.'templates/admin.php';
    }

    public function add_custom_field(){
    	echo "Sub Menu";
    }
}