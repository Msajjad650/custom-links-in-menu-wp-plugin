<?php
/**
 * @package PhloxPlugin
 */

 namespace Inc\Base;

 class SettingsLinks
 {
 	public function register(){
 		add_filter( 'plugin_action_links_'.PLUGIN,array($this, 'setting_links'));
 	}

 	public function setting_links($links){
 		$settings_link = '<a href="admin.php?page=phlox_plugin">Settings</a>';
	    array_push($links, $settings_link);
	    return $links;
 	}
 }