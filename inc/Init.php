<?php
/**
 * @package PhloxPlugin
 */

namespace Inc;
final class Init{
	public static function get_services(){
		return array(
			Pages\Admin::class,
			Base\Enqueue::class,
			Base\SettingsLinks::class
		);
	}
	public static function register_services(){
		foreach (self::get_services() as $class) {
			$service = self::instantiate($class);
			if (method_exists($service, 'register')) {
				$service->register();
			}
		}
	}

	private static function instantiate($class){
		$service = new $class();
		return $service;
	}
}

/*use Inc\Activate;
use Inc\Deactivate;
use Inc\Admin\AdminPages;
if(!class_exists('SajjadPlugin')){

	class SajjadPlugin{
		public $plugin;
	    function __construct(){
	    	$this->plugin = plugin_basename( __FILE__ );
	    }

	    function register(){
	    	add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );
	    	add_action( 'admin_menu', array($this, 'add_admin_pages'));
	    	add_filter( "plugin_action_links_$this->plugin",array($this, 'setting_links'));
	    }

	    function setting_links($links){
	    	$settings_link = '<a href="admin.php?page=sajjad_plugin">Settings</a>';
	    	array_push($links, $settings_link);
	    	return $links;
	    }

	    function add_admin_pages(){
	    	add_menu_page( 'Sajjad Plugin', 'Sajjad', 'manage_options', 'sajjad_plugin', array($this, 'admin_index'), 'dashicons-store', 110 );
	    }

	    function admin_index(){
	    	require_once plugin_dir_path( __FILE__ ).'templates/admin.php';
	    }

	    function activates(){
	        Activate::activated();
	    }

	    function deactivate(){
	    	Deactivate::deactivate();
	    }

	    function custom_post_type(){
	    	register_post_type( 'book', ['public' => true, 'label' => 'Books'] );
	    }

	    function enqueue(){
	    	wp_enqueue_style( 'mypluginstyle', plugins_url('/assets/style.css', __FILE__));
	    	wp_enqueue_style( 'mypluginscript', plugins_url('/assets/script.js', __FILE__));
	    }
	}
}

if(class_exists('SajjadPlugin')){
	$plugin = new SajjadPlugin();
	$plugin->register();
}
*/
//register_activation_hook( __FILE__, array($plugin, 'activates') );
//register_deactivation_hook( __FILE__, array($plugin, 'deactivate') );